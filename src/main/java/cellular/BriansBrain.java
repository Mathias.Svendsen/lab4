
package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    



	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO EDITED
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO EDITED
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO EDITED
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO EDITED
		for (int row=0; row < numberOfRows(); row++) {
			for (int col=0; col < numberOfColumns(); col++) {
				nextGeneration.set(row,col, getNextCell(row,col));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// TODO
		CellState cellCurrent = getCellState(row, col);
		int neighbours = countNeighbors(row, col, CellState.ALIVE);

		if (cellCurrent == CellState.ALIVE) { 
				return CellState.DYING;
			} 
                if (cellCurrent == CellState.DYING) {
                    return CellState.DEAD;
                }
            
			if (neighbours == 2 && cellCurrent == CellState.DEAD) {
				return CellState.ALIVE;
			}
			return CellState.DEAD;
		}


	private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int counter = 0;
		for (int r = row-1;r < row + 2; r++) {
			for (int c = col-1; c < col + 2; c++) {
				if ((r > currentGeneration.numRows()) || (c > currentGeneration.numColumns() )) {
					continue;
				}
				if (r < 0 || c < 0) {
					continue;
				}
				if (r == row && c == col) {
					continue;
				}
				if (state == currentGeneration.get(r,c)) {
					counter++;
				}
				
			}
		}
		return counter;
	}

	@Override 
	public IGrid getGrid() {
		return currentGeneration;
	}
}
