package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int cols;
    int rows;
    CellState[][] csValues;


    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub // EDITED
        this.rows = rows;
        this.cols = columns;
        this.csValues = new CellState[rows][columns];

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub // EDITED
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub // EDITED
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // YODO Auto-generated method stub // EDITED
        if (row >=0 && row<numRows() && column>=0 && column<numColumns()) {
            csValues[row][column] = element;
        }
        else {
            throw new IndexOutOfBoundsException(); 
                }
         }
        
         
    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub // EDITED
        return csValues[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid aCopy;
        aCopy = new CellGrid(rows, cols, CellState.DEAD);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                aCopy.set(i, j, csValues[i][j]);
            }
        }
        return aCopy;
    }
    
}
